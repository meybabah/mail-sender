package com.example.emailsender.metiers;

public interface EmailSenderMetier {

    public void sendEmail(String toEmail, String subject, String message) ;
}
