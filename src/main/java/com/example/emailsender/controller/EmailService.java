package com.example.emailsender.controller;

import com.example.emailsender.entities.EmailMessage;
import com.example.emailsender.metiers.EmailSenderMetier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmailService {

    private final EmailSenderMetier emailSenderService ;

    public EmailService(EmailSenderMetier emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @PostMapping("/send-email")
    public ResponseEntity sendEmail(@RequestBody EmailMessage emailMessage){

            this.emailSenderService.sendEmail(emailMessage.getToEmail(), emailMessage.getSubject(), emailMessage.getMessage());
        return ResponseEntity.ok("succes");


    }

}
